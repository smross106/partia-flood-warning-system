# CUED Part IA Flood Warning System

This is the Part IA Lent Term computing activity at the Department of
Engineering, University of Cambridge.

The activity is documented at
https://cued-partia-flood-warning.readthedocs.io/. Fork this repository
to start the activity.

# Additional Functionality Beyond Scope of Task

The main additional functionality of this module is a Twitter bot that produces a summary of the state of the alert system, as well as specifically mentioning any stations deemed to be at high risk. The most recent activation of this bot was in February 16th, during the peak of Storm Dennis when there was widespread flooding across the country. At this time, the output of the warning system was observed to match that produced by the Environement Agency and other official flood warning systems. This verifies the thresholds we chose for damage potential and risk as being roughly in line with these official sources.

The output of this bot can be viewed at https://twitter.com/AlertsFlood. The summary can be activated with a single function, as shown in Task_2G.py

In addition to this, the module provides a summary of flood risk to various stations in the UK in the command line, and checks flood risk at the Cambridge station.

All of this functionality is demonstrated in a single file, Task_2G.py

# Dependencies

numpy 
    1.18.1

matplotlib
    3.0.3
    
datetime

os

twitter
    3.5
