# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""Unit test for the station module"""

from floodsystem.station import MonitoringStation, inconsistent_typical_range_stations
from floodsystem.stationdata import build_station_list, update_water_levels


def test_create_monitoring_station():

    # Create a station
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (-2.0, 4.0)
    trange = (-2.3, 3.4445)
    river = "River X"
    town = "My Town"
    s = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    assert s.station_id == s_id
    assert s.measure_id == m_id
    assert s.name == label
    assert s.coord == coord
    assert s.typical_range == trange
    assert s.river == river
    assert s.town == town

def test_inconsistent_typical_range():

    #Create a few deliberately false stations

    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (-2.0, 4.0)
    trange = (-2.3, 3.4445)
    river = "River X"
    town = "My Town"
    s1 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)
    s2 = MonitoringStation(s_id, m_id, label, coord, "a", river, town)
    s3 = MonitoringStation(s_id, m_id, label, coord, str(trange), river, town)
    s4 = MonitoringStation(s_id, m_id, label, coord, (-2.3,"a"), river, town)
    s5 = MonitoringStation(s_id, m_id, label, coord, [-2.3,3.445], river, town)
    s6 = MonitoringStation(s_id, m_id, label, coord, (3.445,-2.3), river, town)

    assert s1.typical_range_consistent() == True
    assert s5.typical_range_consistent() == True

    assert s2.typical_range_consistent() == False
    assert s3.typical_range_consistent() == False
    assert s4.typical_range_consistent() == False
    assert s6.typical_range_consistent() == False

def test_inconsistent_typical_range_stations():

    #Create a few deliberately false stations
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (-2.0, 4.0)
    trange = (-2.3, 3.4445)
    river = "River X"
    town = "My Town"
    s1 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)
    s2 = MonitoringStation(s_id, m_id, label, coord, "a", river, town)
    s3 = MonitoringStation(s_id, m_id, label, coord, str(trange), river, town)
    s4 = MonitoringStation(s_id, m_id, label, coord, (-2.3,"a"), river, town)
    s5 = MonitoringStation(s_id, m_id, label, coord, [-2.3,3.445], river, town)
    s6 = MonitoringStation(s_id, m_id, label, coord, (3.445,-2.3), river, town)

    assert inconsistent_typical_range_stations([]) == []

    assert type(inconsistent_typical_range_stations("a")) == TypeError
    assert inconsistent_typical_range_stations(["a"]) == []
    assert inconsistent_typical_range_stations([s2]) == [s2]
    
    assert inconsistent_typical_range_stations([s1,s5]) == []

def test_relative_water_level():
    stations = build_station_list()

    # Update latest level data for all stations
    update_water_levels(stations)

    for station in stations:
        assert type(station.relative_water_level()) == float or station.relative_water_level()==None
