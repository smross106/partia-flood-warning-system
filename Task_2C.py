from floodsystem.stationdata import build_station_list
from floodsystem.flood import stations_highest_rel_level
from floodsystem.station import inconsistent_typical_range_stations
from floodsystem.station import station_name

def run():
    """Requirements for Task 2C"""

    # Build list of stations
    stations = build_station_list()
    sorted_stations = stations_highest_rel_level(stations, 10)
    


    
    for station in sorted_stations:
        print(station[0].name, station[1])
    
if __name__ == "__main__":
    print("*** Task 2C: CUED Part IA Flood Warning System ***")
    run()