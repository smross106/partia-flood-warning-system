"""Unit test for the utils module"""

import floodsystem.utils


def test_sort():
    """Test sort container by specific index"""

    a = (10, 3, 3)
    b = (5, 1, -1)
    c = (1, -3, 4)
    list0 = (a, b, c)

    # Test sort on 1st entry
    list1 = floodsystem.utils.sorted_by_key(list0, 0)
    assert list1[0] == c
    assert list1[1] == b
    assert list1[2] == a

    # Test sort on 2nd entry
    list1 = floodsystem.utils.sorted_by_key(list0, 1)
    assert list1[0] == c
    assert list1[1] == b
    assert list1[2] == a

    # Test sort on 3rd entry
    list1 = floodsystem.utils.sorted_by_key(list0, 2)
    assert list1[0] == b
    assert list1[1] == a
    assert list1[2] == c


def test_reverse_sort():
    """Test sort container by specific index (reverse)"""

    a = (10, 3, 3)
    b = (5, 1, -1)
    c = (1, -3, 4)
    list0 = (a, b, c)

    # Test sort on 1st entry
    list1 = floodsystem.utils.sorted_by_key(list0, 0, reverse=True)
    assert list1[0] == a
    assert list1[1] == b
    assert list1[2] == c

    # Test sort on 2nd entry
    list1 = floodsystem.utils.sorted_by_key(list0, 1, reverse=True)
    assert list1[0] == a
    assert list1[1] == b
    assert list1[2] == c

    # Test sort on 3rd entry
    list1 = floodsystem.utils.sorted_by_key(list0, 2, reverse=True)
    assert list1[0] == c
    assert list1[1] == a
    assert list1[2] == b

def test_is_numeric():
    #Test some values that should return true
    assert floodsystem.utils.is_numeric(1) == True
    assert floodsystem.utils.is_numeric(1.1) == True
    assert floodsystem.utils.is_numeric(-1) == True
    assert floodsystem.utils.is_numeric(65536**2) == True
    assert floodsystem.utils.is_numeric(400.1) == True

    #Test asome values that should return false
    assert floodsystem.utils.is_numeric("a") == False
    assert floodsystem.utils.is_numeric(1+1j) == False
    assert floodsystem.utils.is_numeric(None) == False
    assert floodsystem.utils.is_numeric([1,2,3]) == False
    assert floodsystem.utils.is_numeric([1]) == False
    assert floodsystem.utils.is_numeric({1:1}) == False
    assert floodsystem.utils.is_numeric("3") == False
    assert floodsystem.utils.is_numeric("3.14") == False

def test_is_tuple_numeric():
    #Doesn't need as much testing, since the majority of the function is handled by is_numeric
    #Test some values that should return true
    assert floodsystem.utils.is_tuple_numeric((1,1)) == True
    assert floodsystem.utils.is_tuple_numeric((1,-1)) == True
    assert floodsystem.utils.is_tuple_numeric((1)) == True
    assert floodsystem.utils.is_tuple_numeric((1,-1,1.1,1,1,65536**2)) == True
    assert floodsystem.utils.is_tuple_numeric((399.1,400.1)) == True

    #Test some values that should return false
    assert floodsystem.utils.is_tuple_numeric(("a",1)) == False
    assert floodsystem.utils.is_tuple_numeric(("1",1)) == False
    assert floodsystem.utils.is_tuple_numeric((None)) == False
    assert floodsystem.utils.is_tuple_numeric((((1),1),1)) == False
