from floodsystem.stationdata import build_station_list
from floodsystem.geo import rivers_with_station
from floodsystem.geo import stations_by_river

def run():
    """Requirements for Task 1B"""

    # Build list of stations
    stations = build_station_list()
    sorted_stations = rivers_with_station(stations)
    sorted_stations.sort()
    list_river_stations = []
    for station in sorted_stations[:10]:
        list_river_stations.append(station)
    print(list_river_stations)

    sorted_by_river = stations_by_river(stations)
    aire = sorted_by_river["River Aire"]
    river_aire = []
    for station in aire:
        river_aire.append(station.name)
    river_aire.sort()
    print(" ")
    print("River Aire")
    print(river_aire)

    cam = sorted_by_river["River Cam"]
    river_cam = []
    for station in cam:
        river_cam.append(station.name)
    river_cam.sort()
    print(" ")
    print("River Cam")
    print(river_cam)
    
    thames = sorted_by_river["River Thames"]
    river_thames = []
    for station in thames:
        river_thames.append(station.name)
    river_thames.sort()
    print(" ")
    print("River Thames")
    print(river_thames)
    


    
    
if __name__ == "__main__":
    print("*** Task 1D: CUED Part IA Flood Warning System ***")
    run()