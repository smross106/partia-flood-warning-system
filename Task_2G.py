from floodsystem.query import check_town, twitter_summary, get_summary, sort_stations

def run():
    stations_risks_damages, safe, moderate, high, severe = sort_stations() #obtains data from sort_stations on damages and risk level
    sorted_stations = [stations_risks_damages, safe, moderate, high, severe] #adds the data to a list
    input("See a summary of the current system")
    get_summary(sorted_filtered=sorted_stations) #prints a summary of which towns are at which risk level
    input("See a detailed summary of current system")
    get_summary(detail=True,sorted_filtered=sorted_stations) #prints a summary with more information about particular water levels

    input("Query the state of Cambridge stations")
    check_town("Cambridge",sorted_filtered=sorted_stations) #prints the risk level of Cambridge

    input("Tweet a summary of current system to @AlertFlood")
    input("Are you sure? Make sure you never put anything on the internet you wouldn't mind your mum seeing") #tweets out a current system using @Alert Flood
    twitter_summary(sorted_filtered=sorted_stations)



if __name__ == "__main__":
    print("*** Task 2G: CUED Part IA Flood Warning System ***")
    run()
