
from floodsystem.stationdata import build_station_list
from floodsystem.plot import plot_water_levels
from floodsystem.flood import stations_highest_rel_level

import datetime

def run():
    """Requirements for Task 2E"""

    # Build list of stations
    stations = build_station_list()
    sorted_stations = stations_highest_rel_level(stations,7) #obtains a list of the 5 stations with highest water level
                                                             # 7 stations were needed because (as of 28/01) two of the top 5 stations had invalid data
                                                             # polling the 7th station means that 5 are still plotted
    station_objects = [] #empty list to contain station objects with highest water level
    for i in sorted_stations:
        station_objects.append(i[0]) #adds station objects to list
    plot_water_levels(station_objects,True,10) #plots the water level over past 10 days for 5 stations with highest water level

    
if __name__ == "__main__":
    print("*** Task 2E: CUED Part IA Flood Warning System ***")
    run()