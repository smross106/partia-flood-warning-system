from floodsystem.stationdata import build_station_list
from floodsystem.geo import stations_within_radius

def run():
    """Requirements for Task 1B"""

    # Build list of stations
    stations = build_station_list()
    sorted_stations = stations_within_radius(stations,(52.2053, 0.1218),10)
    within_ten_k = []
    for station in sorted_stations:
        within_ten_k.append(station.name)
    within_ten_k.sort()
    print(within_ten_k)
    
if __name__ == "__main__":
    print("*** Task 1C: CUED Part IA Flood Warning System ***")
    run()