"""Unit test for the station module"""

from floodsystem.station import MonitoringStation
import floodsystem.geo
import unittest
import floodsystem.utils

#Make artificial stations to test various error issues
station_noname =    MonitoringStation("T000","M000",None,(0.1,0.0),(0.1,0.9),"Babbling Brook","Fictionville")
station_nocoord =   MonitoringStation("T001","M001","Harwell ",None,(0.1,0.9),"Babbling Brook","Fictionville")
station_badcoord =   MonitoringStation("T002","M002","Harwell ",(399.1,400.1),(0.1,0.9),"Babbling Brook","Fictionville")
station_norange =   MonitoringStation("T003","M003","Harwell",(-3.5,5.1),None,"Babbling Brook","Fictionville")
station_badrange =  MonitoringStation("T004","M004","Harwell",(2.2,-1.5),(0.9,0.1),"Babbling Brook","Fictionville")
station_noriver =   MonitoringStation("T005","M005","Harwell",(-5.9,0.2),(0.1,0.9),None,"Fictionville")
station_notown =    MonitoringStation("T006","M006","Harwell",(1.4,-5.7),(0.1,0.9),"Babbling Brook",None)

station_ok1 =       MonitoringStation("T007","M007","Kipthorpe",(-1.2,1.1),(0.1,0.9),"Babbling Brook","Fakefield")
station_ok2 =       MonitoringStation("T008","M008","Hencaster",(2.2,1.1),(0.1,0.9),"Babbling Brook","Fictionville")
station_ok3 =       MonitoringStation("T009","M009","Harwell",(3.0,4.0),(0.1,0.9),"Babbling Brook","Fictionville")
station_ok4 =       MonitoringStation("T010","M010","Ashwike",(-4.99,2.99),(0.1,0.9),"Big River","Fictionville")
station_ok5 =       MonitoringStation("T011","M011","Middleburgh",(11.0,12.0),(0.1,0.9),"Big River","Falsetown")
station_ok6 =       MonitoringStation("T012","M012","Woolmere",(11.0,12.0),(0.1,0.9),"Fish Creek","Falsetown")



stationsall = [station_noname,station_nocoord,station_badcoord,station_norange,station_badrange,station_noriver,station_notown,
station_ok1,station_ok2,station_ok3,station_ok4,station_ok5,station_ok6]

def test_stations_by_distance():
    stationsgood = [station_noname,station_norange,station_badrange,station_noriver,station_notown,
station_ok1,station_ok2,station_ok3,station_ok4,station_ok5,station_ok6]

    assert floodsystem.geo.stations_by_distance([],(0.,0.)) == []
    
    assert floodsystem.geo.stations_by_distance([2],(0.,0.)) == []
    assert floodsystem.geo.stations_by_distance([station_nocoord],(0.,0.)) == []
    assert floodsystem.geo.stations_by_distance([station_badcoord],(0.,0.)) == []

    assert type(floodsystem.geo.stations_by_distance(stationsgood,None)) == TypeError
    assert type(floodsystem.geo.stations_by_distance(stationsgood,(200,1))) == ValueError

    assert floodsystem.geo.stations_by_distance(stationsgood,(0,0))[0][0] == station_noname
    assert int(floodsystem.geo.stations_by_distance(stationsgood,(0,0))[0][1]) == 11
    assert floodsystem.geo.stations_by_distance(stationsgood,(0,0))[-1][0] == station_ok6
    assert int(floodsystem.geo.stations_by_distance(stationsgood,(0,0))[-1][1]) == 1804

def test_stations_within_radius():
    stationsgood = [station_noname,station_norange,station_badrange,station_noriver,station_notown,
station_ok1,station_ok2,station_ok3,station_ok4,station_ok5,station_ok6]

    assert floodsystem.geo.stations_within_radius([],(0.,0.),10) == []

    assert type(floodsystem.geo.stations_within_radius([station_nocoord],(0.,0.),10)) == list
    assert type(floodsystem.geo.stations_within_radius([station_badcoord],(0.,0.),10)) == list
    assert type(floodsystem.geo.stations_within_radius(stationsgood,(0.,0.),"a")) == TypeError
    assert type(floodsystem.geo.stations_within_radius(stationsgood,(200.,0.),10)) == ValueError


    assert floodsystem.geo.stations_within_radius(stationsgood,(0.,0.),0) == []
    assert floodsystem.geo.stations_within_radius(stationsgood,(0.,0.),12)[0] == station_noname
    assert len(floodsystem.geo.stations_within_radius(stationsgood,(0.,0.),9999)) == len(stationsgood)

def test_rivers_with_stations():
    stationsgood = [station_noname,station_nocoord,station_badcoord,station_norange,station_badrange,station_notown,
station_ok1,station_ok2,station_ok3,station_ok4,station_ok5,station_ok6]


    assert floodsystem.geo.rivers_with_station([]) == []

    assert floodsystem.geo.rivers_with_station([station_noriver]) == []
    assert type(floodsystem.geo.rivers_with_station("a")) == TypeError
    assert floodsystem.geo.rivers_with_station(["a"]) == []

    assert sorted(floodsystem.geo.rivers_with_station(stationsgood))[0] == "Babbling Brook" 
    assert sorted(floodsystem.geo.rivers_with_station(stationsgood))[-1] == "Fish Creek" 

def test_stations_by_river():
    stationsgood = [station_noname,station_nocoord,station_badcoord,station_norange,station_badrange,station_notown,
station_ok1,station_ok2,station_ok3,station_ok4,station_ok5,station_ok6]

    assert floodsystem.geo.stations_by_river([]) == {}

    assert floodsystem.geo.stations_by_river([station_noriver]) == {}
    assert type(floodsystem.geo.stations_by_river({"a":"b"})) == TypeError

    assert floodsystem.geo.stations_by_river(stationsgood)["Fish Creek"] == [station_ok6]


def test_rivers_by_station_number():
    stationsgood = [station_noname,station_nocoord,station_badcoord,station_norange,station_badrange,station_notown,
station_ok1,station_ok2,station_ok3,station_ok4,station_ok5,station_ok6]

    assert floodsystem.geo.rivers_by_station_number([],1) == []

    assert floodsystem.geo.rivers_by_station_number([station_noriver],1) == []
    assert floodsystem.geo.rivers_by_station_number(["a"],1) == []
    assert type(floodsystem.geo.rivers_by_station_number("a",1)) == TypeError
    assert type(floodsystem.geo.rivers_by_station_number(stationsgood,"a")) == TypeError
    assert type(floodsystem.geo.rivers_by_station_number(stationsgood,-1)) == ValueError


    assert floodsystem.geo.rivers_by_station_number(stationsgood,0) == [('Babbling Brook', 9)]
    assert floodsystem.geo.rivers_by_station_number(stationsgood,999)[-1] == ("Fish Creek",1)
