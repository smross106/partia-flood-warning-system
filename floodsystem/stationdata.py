# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module provides interface for extracting statiob data from
JSON objects fetched from the Internet and

"""
try:
    import datafetcher
    from station import MonitoringStation
    from utils import is_numeric
except ModuleNotFoundError:
    from floodsystem import datafetcher 
    from floodsystem.station import MonitoringStation
    from floodsystem.utils import is_numeric

import datetime

def build_station_list(use_cache=True):
    """Build and return a list of all river level monitoring stations
    based on data fetched from the Environment agency. Each station is
    represented as a MonitoringStation object.

    The available data for some station is incomplete or not
    available.

    """

    # Fetch station data
    data = datafetcher.fetch_station_data(use_cache)

    # Build list of MonitoringStation objects
    stations = []
    for e in data["items"]:
        # Extract town string (not always available)
        town = None
        if 'town' in e:
            town = e['town']

        # Extract river name (not always available)
        river = None
        if 'riverName' in e:
            river = e['riverName']

        # Attempt to extract typical range (low, high)
        try:
            typical_range = (float(e['stageScale']['typicalRangeLow']),
                             float(e['stageScale']['typicalRangeHigh']))
        except Exception:
            typical_range = None

        try:
            # Create mesure station object if all required data is
            # available, and add to list
            s = MonitoringStation(
                station_id=e['@id'],
                measure_id=e['measures'][-1]['@id'],
                label=e['label'],
                coord=(float(e['lat']), float(e['long'])),
                typical_range=typical_range,
                river=river,
                town=town)
            stations.append(s)
        except Exception:
            # Not all required data on the station was available, so
            # skip over
            pass

    return stations


def update_water_levels(stations):
    """Attach level data contained in measure_data to stations"""

    # Fetch level data
    measure_data = datafetcher.fetch_latest_water_level_data()

    # Build map from measure id to latest reading (value)
    measure_id_to_value = dict()
    for measure in measure_data['items']:
        if 'latestReading' in measure:
            latest_reading = measure['latestReading']
            measure_id = latest_reading['measure']
            measure_id_to_value[measure_id] = latest_reading['value']

    # Attach latest reading to station objects
    for station in stations:

        # Reset latestlevel
        station.latest_level = None

        # Atach new level data (if available)
        if station.measure_id in measure_id_to_value:
            if isinstance(measure_id_to_value[station.measure_id], float):
                station.latest_level = measure_id_to_value[station.measure_id]

def update_historic_water_levels(stations,dt):
    """
    Adds properly checked historic level data (dates and levels at corresponding dates) to stations
    
    Args:
        stations:           list of MonitoringStations
        dt                  number of days of data to recover
    """
    if not is_numeric(dt):
        return(TypeError)
    if dt<0:
        return(ValueError)
    if type(stations)!=list:
        return(TypeError)

    for station in stations:
        if type(station)!=MonitoringStation:
            print(TypeError)
            continue
        dates, levels = datafetcher.fetch_measure_levels(station.measure_id, dt=datetime.timedelta(days=dt))
        if type(dates)!=list or type(levels)!=list:
            return(TypeError)
        if not (all(type(d)==datetime.datetime for d in dates) and all(type(l)==float for l in levels)):
            print(TypeError)
            continue
        if len(dates)!=len(levels):
            print(ValueError("Length of dates and levels does not match up: "+station.name))
            continue
        
        station.historic_dates = dates
        station.historic_levels = levels

