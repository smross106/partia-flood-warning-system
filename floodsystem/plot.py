"""
Plots level data from MonitoringStations
"""

import matplotlib.pyplot as plt
from matplotlib import dates as mpdates
from datetime import datetime, timedelta, date
from numpy import poly1d

try:
    from utils import is_tuple_numeric, is_numeric, haversine
    from station import MonitoringStation
    from stationdata import update_historic_water_levels
    from analysis import levels_polyfit
except ModuleNotFoundError:
    from floodsystem.utils import is_tuple_numeric, is_numeric, haversine
    from floodsystem.station import MonitoringStation
    from floodsystem.stationdata import update_historic_water_levels
    from floodsystem.analysis import levels_polyfit


def plot_water_levels(stations,update=False,dt=10):
    """
    Plots historic water levels and typical range of one or many stations

    Args:
        stations:               MonitoringStation
                                    OR
                                list of MonitoringStation objects
        update (optional)       Boolean, default false
                                    if False, uses existing level data and ignores any incomplete stations
                                    if True, gets new level data
        dt (optional)           number of days of data to recover
    
    Ouput:
        Plots the historic level data from station[i] at dates with a matplotlib graph
        Graph also shows typical high and low for station

    Raises:
        ValueErrror             stations does not have valid historical data (IF a single station is being plotted)
        TypeError               stations is not a MonitoringStation or list of MonitoringStations
                                
    """
    valid_stations = []
    if type(stations)==MonitoringStation:
        if update:
            update_historic_water_levels([stations],dt)

        if len(stations.historic_levels)!=0 and len(stations.historic_dates)!=0:
            valid_stations.append(stations)
        else:
            return(ValueError("station does not have valid historic data"))
    
    elif type(stations)==list:
        if update:
            update_historic_water_levels(stations,dt)

        for i in stations:
            if len(i.historic_levels)!=0 and len(i.historic_dates)!=0:
                valid_stations.append(i)
            else:
                print(ValueError("station does not have valid historic data "+i.name))
    
    else:
        return(TypeError("stations is not a MonitoringStation or list of MonitoringStations"))
    
    if len(valid_stations)==0:
        print("No valid stations")
    
    else:
        print("Plotting now")
        n_rows = int((len(valid_stations)+1)/2)

        for i in range(0,len(valid_stations)):
            if len(valid_stations)>=2:
                plt.subplot(n_rows,2,i+1)

            plt.plot(valid_stations[i].historic_dates,valid_stations[i].historic_levels)

            plt.plot([valid_stations[i].historic_dates[0],valid_stations[i].historic_dates[-1]],
            [valid_stations[i].typical_range[0],valid_stations[i].typical_range[0]],
            linestyle=':')

            plt.plot([valid_stations[i].historic_dates[0],valid_stations[i].historic_dates[-1]],
            [valid_stations[i].typical_range[1],valid_stations[i].typical_range[1]],
            linestyle=':')

            plt.xlabel('date')
            plt.ylabel('water level (m)')
            plt.xticks(rotation=45)
            plt.title(valid_stations[i].name)
        plt.tight_layout(pad=0)
        plt.show()


def plot_water_levels_with_fit(stations,update=False,dt=10,p=3):
    """
    Plots historic water levels and typical range of one or many stations

    Args:
        stations:               MonitoringStation
                                    OR
                                list of MonitoringStation objects
        update (optional)       Boolean, default false
                                    if False, uses existing level data and ignores any incomplete stations
                                    if True, gets new level data
        dt (optional)           number of days of data to recover
        p (optional)            integer, order of polynomial
    
    Ouput:
        Plots the historic level data from station[i] at dates with a matplotlib graph
        Graph also shows typical high and low for station

    Raises:
        ValueErrror             stations does not have valid historical data (IF a single station is being plotted)
        TypeError               stations is not a MonitoringStation or list of MonitoringStations
                                
    """
    valid_stations = []
    if type(stations)==MonitoringStation:
        if update:
            update_historic_water_levels([stations],dt)
            levels_polyfit(stations,p)

        if len(stations.historic_levels)!=0 and len(stations.historic_dates)!=0:
            if type(stations.p0)==poly1d and type(stations.offset)!=None:
                valid_stations.append(stations)
            else:
                return(ValueError("station does not have valid polynomial data"))
        else:
            return(ValueError("station does not have valid historic data"))
    
    elif type(stations)==list:
        if update:
            update_historic_water_levels(stations,dt)

        for i in stations:
            if update:
                levels_polyfit(i,p)

            if len(i.historic_levels)!=0 and len(i.historic_dates)!=0:
                if type(i.p0)==poly1d and type(i.offset)!=None:
                    valid_stations.append(i)
                else:
                    return(ValueError("station does not have valid polynomial data: "+i.name))
            else:
                print(ValueError("station does not have valid historic data " + i.name))
    
    else:
        return(TypeError("stations is not a MonitoringStation or list of MonitoringStations"))
    
    if len(valid_stations)==0:
        print("No valid stations")
    
    else:
        print("Plotting now")
        n_rows = int((len(valid_stations)+1)/2)

        for i in range(0,len(valid_stations)):
            if len(valid_stations)>=2:
                plt.subplot(n_rows,2,i+1)
            
            trendline = []
            for date in valid_stations[i].historic_dates:
                num = mpdates.date2num(date) - valid_stations[i].offset
                trendline.append(valid_stations[i].p0(num))
            
            plt.plot(valid_stations[i].historic_dates,trendline,color='red')

            plt.plot(valid_stations[i].historic_dates,valid_stations[i].historic_levels)

            plt.plot([valid_stations[i].historic_dates[0],valid_stations[i].historic_dates[-1]],
            [valid_stations[i].typical_range[0],valid_stations[i].typical_range[0]],
            linestyle=':')

            plt.plot([valid_stations[i].historic_dates[0],valid_stations[i].historic_dates[-1]],
            [valid_stations[i].typical_range[1],valid_stations[i].typical_range[1]],
            linestyle=':')

            plt.xlabel('date')
            plt.ylabel('water level (m)')
            plt.xticks(rotation=45)
            plt.title(valid_stations[i].name)
        plt.tight_layout(pad=0)
        plt.show()

        