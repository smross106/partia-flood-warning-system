"""
Analysis code for historic data for MonitoringStations
"""
from numpy import poly1d, polyfit, polyder
from matplotlib.dates import date2num
import datetime

try:
    from flood import stations_highest_rel_level,stations_level_over_threshold
    from geo import stations_by_river, stations_within_radius
    from station import MonitoringStation 
except ModuleNotFoundError:
    from floodsystem.flood import stations_highest_rel_level,stations_level_over_threshold
    from floodsystem.geo import stations_by_river, stations_within_radius
    from floodsystem.station import MonitoringStation 

def levels_polyfit(station, p):
    """
    gets a least-squares best fit polynomial of order p
    """
    if type(p)!=int:
        return(TypeError("p is not an integer"))
    if p<0:
        return(ValueError("p is negative"))
    if type(station)!=MonitoringStation:
        return(TypeError("station is not a MonitoringStation"))
    if len(station.historic_dates)==0 or len(station.historic_levels)==0:
        return(ValueError("Station does not have historic data"))
    if len(station.historic_dates)!=len(station.historic_levels):
        return(ValueError("Station does not have valid historic data"))
        

    adjusted_dates = []
    offset = date2num(station.historic_dates[0])

    for i in station.historic_dates:
        adjusted_dates.append(date2num(i) -offset)
    
    coeffs = polyfit(adjusted_dates,station.historic_levels,p)

    p0 = poly1d(coeffs)

    station.p0 = p0
    station.offset = offset


def flood_risk_level(stations):
    """
    give each station a flood risk score, based on how far above normal range it is
    formula:    if level<typical maximum, score = 0
                if level>typical maximum, score = (amount over typical max)**2
    """
    stations_levels = stations_level_over_threshold(stations,0.8)
    risks = {}
    for i in stations_levels:
        station = i[0]
        if i[1]<0.8:
            risks[station] = 0
        else:
            risks[station] = i[1]**2

    return(risks)

def flood_risk_change(stations,update=False,p=3):
    """
    give each station a flood risk score, based on how quickly it is rising
    formula:    if 1st derivative is +ve, risk=  (level')**2 * (level'')
                if 1st derivative is -ve, risk= -(level')**2 * (level'')
    """
    if update:
        for i in stations:
            levels_polyfit(i,p)
    
    risks = {}

    for i in stations:
        if len(i.historic_dates)==0 or i.offset==None:
            risks[i] = 0
            continue
        current_time = date2num(i.historic_dates[-1])-i.offset
        if type(i.p0)!=poly1d:
            print(ValueError("station does not have valid polynomial data"))
            risks[i] = 0
            continue
        mag_change_coeff = (polyder(i.p0,1)(current_time)**2)*(polyder(i.p0,2)(current_time))
        if polyder(i.p0,1)(current_time)<0:
            mag_change_coeff = -mag_change_coeff
        risks[i] = mag_change_coeff
    
    return(risks)
        
        
def flood_damage(stations):
    """
    assigns a crude damage predictor for a given station
    formula: number of stations on river + number of stations within 5km radius
    """
    damage = {}

    rivers = stations_by_river(stations)

    for station in stations:
        if type(station)!=MonitoringStation:
            damage[station]=0
            continue
        #within_range = len(stations_within_radius(stations, station.coord, 5))
        on_river = len(rivers[station.river])
        damage[station] = on_river
    
    return(damage)


