import twitter
import os
import datetime

a = open(os.path.expanduser('~/.config/bot_keys.txt'))
keys = a.read().split(";")

api = twitter.Api(consumer_key = keys[0], consumer_secret = keys[1], 
                  access_token_key = keys[2], access_token_secret = keys[3])



def tweet_message(message):
    try:
        status = api.PostUpdate(message)
        print("Tweet beginning %(msg)s sent" % {'msg':message[0:10]})
    except UnicodeDecodeError:
        print("Your message could not be encoded.  Perhaps it contains non-ASCII characters? ")

def check_last_message():
    try:
        last_message = api.GetHomeTimeline(count=1)
        timestamp_string = last_message[0].created_at
        timestring = timestamp_string.split(" ")[3]
        monthstring = timestamp_string.split(" ")[1]
        daystring = timestamp_string.split(" ")[2]
        yearstring = timestamp_string.split(" ")[5]
        print(timestring,daystring,monthstring,yearstring)
    except ValueError:
        print("Retrival of message failed")