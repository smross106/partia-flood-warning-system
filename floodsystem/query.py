"""
Query the flood system
(final frontend)
"""
from datetime import datetime, time, date
try:
    from alert import get_risks_damages
    from tweet import tweet_message
except ModuleNotFoundError:
    from floodsystem.alert import get_risks_damages
    from floodsystem.tweet import tweet_message

def sort_stations():
    stations_risks_damages = get_risks_damages()

    safe = []
    moderate = []
    high = []
    severe = []

    for station in stations_risks_damages:
        r_d = stations_risks_damages[station]
        if r_d[0]<0.5:
            safe.append(station)
        elif r_d[0]<4:
            moderate.append(station)
        elif r_d[0]<8 and r_d[1]<5:
            moderate.append(station)
        elif r_d[0]<8:
            high.append(station)
        elif r_d[1]<5:
            moderate.append(station)
        elif r_d[1]<10:
            high.append(station)
        else:
            severe.append(station)

    return(stations_risks_damages,safe,moderate,high,severe)

def get_summary(detail=False,sorted_filtered=None):
    if type(sorted_filtered)==list:
        stations_risks_damages = sorted_filtered[0]
        safe = sorted_filtered[1]
        moderate = sorted_filtered[2]
        high = sorted_filtered[3]
        severe = sorted_filtered[4]
    else:
        stations_risks_damages, safe, moderate, high, severe = sort_stations()
        

    if detail:
        moderate_all_names = ""
        for i in moderate:
            moderate_all_names+=(i.name+", ")
        moderate_all_names=moderate_all_names[0:-2]

    high_all_names = ""
    for i in high:
        high_all_names+=(i.name+", ")
    high_all_names=high_all_names[0:-2]

    severe_all_names = ""
    for i in severe:
        severe_all_names+=(i.name+", ")
    severe_all_names=severe_all_names[0:-2]

    message = []
    message.append("*** *** CUED Part IA Flood Warning System - Current State Summary *** ***")
    message.append("System has gathered data from %(accurate)s stations " % \
        {"accurate":len(safe)+len(moderate)+len(high)+len(severe)})
    message.append(("%(safe)s reported no flood risk" % \
    {'safe':len(safe)}))

    message.append("")

    message.append("%(moderate)s stations reported a moderate flood risk: flooding is possible, be prepared" % \
        {"moderate":len(moderate)})
    if detail:
        message.append("Stations at moderate risk are: "+moderate_all_names)
    
    message.append("")
    
    message.append("%(high)s stations reported a high flood risk: flooding is expected, immediate action required" % \
        {"high":len(high)})
    if len(high)!=0:
        message.append("Stations at high risk are: "+high_all_names)

    message.append("")

    message.append("%(severe)s stations reported a severe flood risk: severe flooding, danger to life" % \
        {"severe":len(severe)})
    if len(severe)!=0:
        message.append("Stations at severe risk are: "+severe_all_names)


    for i in message:print(i)

def twitter_summary(sorted_filtered=None):
    if type(sorted_filtered)==list:
        stations_risks_damages = sorted_filtered[0]
        safe = sorted_filtered[1]
        moderate = sorted_filtered[2]
        high = sorted_filtered[3]
        severe = sorted_filtered[4]
    else:
        stations_risks_damages, safe, moderate, high, severe = sort_stations()
    time_now = datetime.now().strftime("%d %b %H:%M")
    
    mod_max_risk = -999
    mod_max_risk_town = ""
    mod_max_dmg = -999
    mod_max_dmg_town = ""
    for station in moderate:
        if stations_risks_damages[station][0]>mod_max_risk:
            mod_max_risk = stations_risks_damages[station][0]
            mod_max_risk_town = station.town
        if stations_risks_damages[station][1]>mod_max_risk:
            mod_max_dmg = stations_risks_damages[station][1]
            mod_max_dmg_town = station.town
    
    mod_max_risk_town = "".join(mod_max_risk_town.split())
    mod_max_dmg_town = "".join(mod_max_dmg_town.split())
    

    tweet1 = ("UK Flood Alerts gathered data from %(accurate)s flood warning stations across the UK at %(datetime)s. Of these stations, %(moderate)s reported a moderate risk, %(high)s reported high risk and %(severe)s reported severe risk. See https://tinyurl.com/flood-alert for more information #ukflood " % \
        {"accurate":len(safe)+len(moderate)+len(high)+len(severe),"datetime":time_now,"moderate":len(moderate),"high":len(high),"severe":len(severe)})
    tweet_message(tweet1)
    
    tweet2 = ("%(moderate)s flood warning stations across the UK, including #%(max_risk_town)s and #%(max_dmg_town)s, are currently reporting a risk of flooding. Check https://tinyurl.com/moderate-flood to see if your area is affected and find out what to do. #ukflood" % \
        {'moderate':len(moderate), 'max_risk_town':mod_max_risk_town, 'max_dmg_town':mod_max_dmg_town})
    tweet_message(tweet2)

    if len(high)!=0:
        for station in high:
            tweet_h = ("! Warning ! \n Flood monitoring stations in %(townname)s have detected high risk of flood. Action is advised for all residents in the affected area, see https://tinyurl.com/flood-alert for advice on what to do. #%(townnamehash)s #%(rivername)s #ukflood" % \
            {'townname':station.town,'townnamehash':"".join((station.town).split()), "rivername":"".join((station.river).split())})
            tweet_message(tweet_h)
    
    if len(severe)!=0:
        for station in severe:
            tweet_s  = ("!! Warning !! \n Flood monitoring stations in %(townname)s have detected severe risk of flood - loss of life is possible. Immediate action is strongly advised, see https://tinyurl.com/severe-flood for advice on what to do. #%(townnamehash)s #%(rivername)s #ukflood " % \
            {'townname':station.town,'townnamehash':"".join((station.town).split()), "rivername":"".join((station.river).split())})
            tweet_message(tweet_s)

def check_town(check,sorted_filtered=None):
    if type(sorted_filtered)==list:
        stations_risks_damages = sorted_filtered[0]
        safe = sorted_filtered[1]
        moderate = sorted_filtered[2]
        high = sorted_filtered[3]
        severe = sorted_filtered[4]
    else:
        stations_risks_damages, safe, moderate, high, severe = sort_stations()

    town = None
    found = False
    i = 1
    while found == False:
        for i in safe:
            if i.town == check:
                print(i.town +" is at low risk")
                found = True
                break
        
        for i in moderate:
            if i.town == check:
                print(i.town +" is at moderate risk")
                found = True
                break
        
        for i in high:
            if i.town == check:
                print(i.town + " is at high risk - take action")
                found = True
                break
        
        for i in severe:
            if i.town==check:
                print(i.town== " is at severe risk - take action immediately to avoid loss of life")
                found = True
                break
        
        if found == False:
            print("Town is not listed, please try again")
            found = True