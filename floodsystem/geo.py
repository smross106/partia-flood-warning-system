# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module contains a collection of functions related to
geographical data.

"""
try:
   from utils import is_tuple_numeric, is_numeric, haversine
   from station import MonitoringStation

except ModuleNotFoundError:
   from floodsystem.utils import is_tuple_numeric, is_numeric, haversine
   from floodsystem.station import MonitoringStation



def stations_by_distance(stations, p):
    """
    Sorts stations by distance to a specified coordinate

    Args:
        stations:               list of MonitoringStation objects
                                    all the stations that should be sorted
        p:                      tuple of floats
                                    coordinates that distance should be measured from
    
    Returns:
        stations_distances      list of tuples (MonitoringStation, float)
                                    all stations and the distance to p, sorted from closest to furthest

    Raises:
        TypeError               p is not a valid numerical coordinate
                                any element of stations is not a valid MonitoringStation
                                any station does not have a valid numerical coordinate
        ValueErrror             p is not a valid lat:long coordinate - |lat|>90 or |long|>180
                                any station has an invalid lat:long coordinate - |lat|>90 or |long|>180
    """

    
    
    if not is_tuple_numeric(p):
        return(TypeError("p is not a valid numerical coordinate"))
    if abs(p[0])>90 or abs(p[1])>180:
        return(ValueError("Invalid coordinate returned"))

    stations_distances = []
    for station in stations:
        if type(station)!=MonitoringStation:
            print(TypeError("Input not a valid MonitoringStation"))
            continue
        if (not is_tuple_numeric(station.coord)):
            print(TypeError("Station does not have a numerical coordinate"))
            continue
        if abs(station.coord[0])>90 or abs(station.coord[1])>180:
            print(ValueError("Station has an invalid coordinate"))
            continue
        
        #Use the Haversine formula to find distance
        distance = haversine(station.coord,p)
        stations_distances.append((station,distance))
    
    stations_distances.sort(key = lambda x: x[1])  

    return(stations_distances)


def stations_within_radius(stations, centre, r):
    """
    Sorts stations by distance to a specified coordinate

    Args:
        stations:               list of MonitoringStation objects
                                    all the stations that should be sorted
        centre:                 tuple of floats
                                    coordinates that distance should be measured from
        r:                      integer or float
                                    maximum distance from centre, in km
    
    Returns:
        stations_distances:     list of MonitoringStation
                                    all stations less than r from centre

    Raises:
        TypeError:              centre is not a valid numerical coordinate
                                any element of stations is not a valid MonitoringStation
                                any station does not have a valid numerical coordinate
                                r is not a valid distance (not numeric or less than zero)
        ValueErrror:            centre is not a valid lat:long coordinate - |lat|>90 or |long|>180
                                any station has an invalid lat:long coordinate - |lat|>90 or |long|>180
        RunTimeError:           centre is not a valid coordinate but has not been caught, so stations_by_distance has returned an error
    """
    if not is_tuple_numeric(centre):
        return(TypeError("centre is not a valid coordinate"))
    if (not is_numeric(r)) or r<0 :
        return((TypeError("r is not a valid numeric distance")))
    if abs(centre[0])>90 or abs(centre[1])>180:
        return(ValueError("Invalid coordinate returned"))


    sorted_stations = stations_by_distance(stations, centre)

    if type(sorted_stations)!=list:
        return(RuntimeError("stations_by_distance has failed, centre is invalid and has escaped checks"))

    stations_in_radius = []

    for station_tuple in sorted_stations:
        if station_tuple[1]<=r:
            stations_in_radius.append(station_tuple[0])

    return(stations_in_radius)


def rivers_with_station(stations):
    """
    Finds every unique river with at least one station

    Args:
        stations:               list of MonitoringStation objects
                                    all the stations that should be sorted
    
    Returns:
        rivers:                 list of strings
                                    name of every river

    Raises:
        TypeError:              stations is not a list
                                any element of stations is not a MonitoringStation
                                any element of stations does not have a valid river name
    """
    if type(stations)!=list:
        return(TypeError("stations is not a list"))

    rivers = set()

    for station in stations:
        if type(station)!=MonitoringStation:
            print(TypeError("one element of stations is not a valid MonitoringStation"))
            continue
        if type(station.river)!=str:
            print(TypeError("River name is not a valid string"))
            continue
        rivers.add(station.river)
    
    return(list(rivers))

def stations_by_river(stations):
    """
    Finds every unique river and all the stations on it

    Args:
        stations:               list of MonitoringStation objects
                                    all the stations that should be sorted
    
    Returns:
        rivers+stations:         dict of {string: [MonitoringStation, MonitoringStation...]}
                                    {name of river: [all stations on that river]}

    Raises:
        TypeError:              stations is not a list
                                any element of stations is not a MonitoringStation
                                any element of stations does not have a valid river name
    """
    if type(stations)!=list:
        return(TypeError("stations is not a list"))

    all_rivers = rivers_with_station(stations)

    rivers_stations = { river : [] for river in all_rivers }
    
    for station in stations:
        if type(station)!=MonitoringStation:
            print(TypeError("one element of stations is not a valid MonitoringStation"))
            continue
        if type(station.river)!=str:
            print(TypeError("River name is not a valid string"))
            continue
        rivers_stations[station.river].append(station)

    return(rivers_stations)

def rivers_by_station_number(stations, N):
    """
    Finds the N rivers with the most stations, returning more than N if several stations tie

    Args:
        stations:                list of MonitoringStation objects
                                     all the stations that should be sorted
        N:                  
    
    Returns:
        rivers_above_threshold: list of tuples (string, int)
                                    list of (river name, number of stations) sorted in descending order

    Raises:
        TypeError:              stations is not a list
                                N is not an integer
        ValueError:             N is not a valid counting number
        RunTimeError            bad input not caught here
    """
    if type(N)!=int:
        return(TypeError("N is not an integer"))
    if N<0:
        return(ValueError("N is negative"))
    if type(stations)!=list:
        return(TypeError("stations is not a list"))

    stations_rivers = stations_by_river(stations)
    if type(stations_rivers)!=dict:
        return(RuntimeError("bad input, passed error to stations_by_river"))

    rivers_number_of_stations = [(river,len(stations_rivers[river])) for river in stations_rivers]
    rivers_number_of_stations.sort(key = lambda x: x[1])  
    rivers_number_of_stations.reverse()

    rivers_above_threshold = []

    current_N = 999
    for river in rivers_number_of_stations:
        if len(rivers_above_threshold) > N:
            if river[1] == current_N:
                rivers_above_threshold.append(river)
            else:
                break
        else:
            rivers_above_threshold.append(river)
            current_N = river[1]
    
    return(rivers_above_threshold)




