# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module provides a model for a monitoring station, and tools
for manipulating/modifying station data

"""
try:
   from utils import is_tuple_numeric, is_numeric

except ModuleNotFoundError:
   from floodsystem.utils import is_tuple_numeric, is_numeric

class MonitoringStation:
    """This class represents a river level monitoring station"""

    def __init__(self, station_id, measure_id, label, coord, typical_range,
                 river, town):

        self.station_id = station_id
        self.measure_id = measure_id

        # Handle case of erroneous data where data system returns
        # '[label, label]' rather than 'label'
        self.name = label
        if isinstance(label, list):
            self.name = label[0]

        self.coord = coord
        self.typical_range = typical_range
        self.river = river
        self.town = town

        self.latest_level = None

        self.historic_levels = []
        self.historic_dates = []

        self.p0 = None
        self.offset = None
    
    def typical_range_consistent(self):
        """
        Checks if the typical range for a station is:
            -a tuple composed only of numbers
            -a tuple of length 2
            -properly ordered
        """
        if not is_tuple_numeric(self.typical_range):
            return(False)
        if len(self.typical_range)!=2:
            return(False)
        if self.typical_range[1]<self.typical_range[0]:
            return(False)
        return(True)

    def relative_water_level(self):
        if not self.typical_range_consistent():
            return(None)
        if not is_numeric(self.latest_level):
            return(None)
        
        return((self.latest_level-self.typical_range[0])/self.typical_range[1])

    def __repr__(self):
        d = "Station name:     {}\n".format(self.name)
        d += "   id:            {}\n".format(self.station_id)
        d += "   measure id:    {}\n".format(self.measure_id)
        d += "   coordinate:    {}\n".format(self.coord)
        d += "   town:          {}\n".format(self.town)
        d += "   river:         {}\n".format(self.river)
        d += "   typical range: {}".format(self.typical_range)
        return d

def inconsistent_typical_range_stations(stations):
    """
    Takes a list of MonitoringStations, returns a list of all with inconsistent ranges
    """
    inconsistent_stations = []
    
    if type(stations)!=list:
        return(TypeError("stations is not a valid list"))
    
    for station in stations:
        if type(station)!=MonitoringStation:
            print(TypeError("Input not a valid MonitoringStation"))
            continue
        if not station.typical_range_consistent():
            inconsistent_stations.append(station)
    
    return(inconsistent_stations)

def station_name(stations):
    """
    Takes a list of Monitoring stations objects or list of tuple of Monitoring station objects, returns a list with Monitoring Station names

    """

    names_of_stations = [] #empty list to contain name of the stations


    if all(isinstance(item,tuple) for item in stations) == True: #checks to see if the input is a list of tuples
        for station in stations:
            if type(station[0])!=MonitoringStation: #checks to see whether the station is an monitoring station, if not it continues
                print(TypeError("Input not a valid MonitoringStation"))
                continue
            names_of_stations.append(station[0].name) #if the input is a list of tuples, adds the station name to the list

    else:
        for station in stations: #if the input is a list of monitoring stations
            names_of_stations.append(station.name) #adds the station name to a list
    
    return names_of_stations #returns a list of the station names