"""
Public flood warning broadcast system using Twitter API
"""

try:
    from analysis import flood_damage, flood_risk_change, flood_risk_level
    from stationdata import build_station_list, update_historic_water_levels
    from flood import stations_level_over_threshold
except ModuleNotFoundError:
    from floodsystem.analysis import flood_damage, flood_risk_change, flood_risk_level
    from floodsystem.stationdata import build_station_list, update_historic_water_levels
    from floodsystem.flood import stations_level_over_threshold


def get_risks_damages():
    stations = build_station_list()
    stations_levels = stations_level_over_threshold(stations,0.8)

    at_risk_stations = [x[0] for x in stations_levels]
    stations_risks_damages = {}
    for i in stations:
        stations_risks_damages[i] = [0,0]

    update_historic_water_levels(at_risk_stations,10)
    print("station data obtained")

    risk1 = flood_risk_change(stations)
    risk2 = flood_risk_level(stations)
    print("risks calculated")
    damage = flood_damage(stations)
    print("damage predictions calculated")

    for i in stations:
        if not (i in at_risk_stations):
            pass
        else:
            riskT = 0
            try:riskT = risk1[i] 
            except KeyError:pass
            try: riskT += risk2[i]
            except KeyError: pass
            damageT = damage[i]
            stations_risks_damages[i] = [riskT,damageT]
    
    return(stations_risks_damages)