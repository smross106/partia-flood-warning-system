try:
   from utils import is_tuple_numeric, is_numeric, haversine
   from station import MonitoringStation
   from stationdata import build_station_list, update_water_levels


except ModuleNotFoundError:
   from floodsystem.utils import is_tuple_numeric, is_numeric, haversine
   from floodsystem.station import MonitoringStation
   from floodsystem.stationdata import build_station_list, update_water_levels


def stations_highest_rel_level(stations, N,update=True):
    """
    Get a list of the N stations with the highest water levels
    """
    if type(N)!=int:
        return(TypeError("N is not an integer "))
    if N<0:
        return(ValueError("N is less than 0, not a valid counting number"))
    if type(stations)!=list:
        return(TypeError("stations is not a list"))

    # Update latest level data for all stations
    if update:
        update_water_levels(stations)

    stations_levels = []
    for station in stations:
        if type(station)!=MonitoringStation:
            print(TypeError("Element of stations is not a MonitoringStation object"))
        rel_level = station.relative_water_level()
        if rel_level == None:
            continue
        stations_levels.append((station, rel_level))
    
    
    stations_levels.sort(key = lambda x: x[1])  
    stations_levels.reverse()

    

    if N==0:
        return([])
    elif N>len(stations_levels):
        return(stations_levels)
    else:
        return(stations_levels[0:N])

def stations_level_over_threshold(stations, tol,update=True):
    """
    Return every station with a relative water level over tol
    """
    if not is_numeric(tol):
        return(TypeError("tol is not an number"))
    if type(stations)!=list:
        return(TypeError("stations is not a list"))
    
    stations_levels = stations_highest_rel_level(stations, len(stations),update)


    stations_above_tol = []

    for station_level in stations_levels:
        if station_level[1]>tol:
            stations_above_tol.append(station_level)
    
    return(stations_above_tol)