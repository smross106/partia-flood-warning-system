from floodsystem.stationdata import build_station_list
from floodsystem.station import inconsistent_typical_range_stations,station_name, MonitoringStation
from floodsystem.analysis import levels_polyfit

station_ok1 =       MonitoringStation("T007","M007","Kipthorpe",(-1.2,1.1),(0.1,0.9),"Babbling Brook","Fakefield")

def test_levels_polyfit():
    assert type(levels_polyfit(station_ok1,None))==TypeError
    assert type(levels_polyfit("a",1)) == TypeError
    assert type(levels_polyfit(station_ok1,-5))==ValueError
    assert type(levels_polyfit(station_ok1,3)) == ValueError

