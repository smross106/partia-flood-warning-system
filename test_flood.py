from floodsystem.stationdata import build_station_list
from floodsystem.flood import stations_level_over_threshold, stations_highest_rel_level
from floodsystem.station import inconsistent_typical_range_stations,station_name, MonitoringStation


station_noname =    MonitoringStation("T000","M000",None,(0.1,0.0),(0.1,0.9),"Babbling Brook","Fictionville")
station_nocoord =   MonitoringStation("T001","M001","Harwell ",None,(0.1,0.9),"Babbling Brook","Fictionville")
station_badcoord =   MonitoringStation("T002","M002","Harwell ",(399.1,400.1),(0.1,0.9),"Babbling Brook","Fictionville")
station_norange =   MonitoringStation("T003","M003","Harwell",(-3.5,5.1),None,"Babbling Brook","Fictionville")
station_badrange =  MonitoringStation("T004","M004","Harwell",(2.2,-1.5),(0.9,0.1),"Babbling Brook","Fictionville")
station_noriver =   MonitoringStation("T005","M005","Harwell",(-5.9,0.2),(0.1,0.9),None,"Fictionville")
station_notown =    MonitoringStation("T006","M006","Harwell",(1.4,-5.7),(0.1,0.9),"Babbling Brook",None)
station_badlevel =   MonitoringStation("T013","M013","Harwell",(-5.9,0.2),(0.1,0.9),None,"Fictionville")
station_nolevel =    MonitoringStation("T014","M014","Harwell",(1.4,-5.7),(0.1,0.9),"Babbling Brook",None)

station_ok1 =       MonitoringStation("T007","M007","Kipthorpe",(-1.2,1.1),(0.1,0.9),"Babbling Brook","Fakefield")
station_ok2 =       MonitoringStation("T008","M008","Hencaster",(2.2,1.1),(0.1,0.9),"Babbling Brook","Fictionville")
station_ok3 =       MonitoringStation("T009","M009","Harwell",(3.0,4.0),(0.1,0.9),"Babbling Brook","Fictionville")
station_ok4 =       MonitoringStation("T010","M010","Ashwike",(-4.99,2.99),(0.1,0.9),"Big River","Fictionville")
station_ok5 =       MonitoringStation("T011","M011","Middleburgh",(11.0,12.0),(0.1,0.9),"Big River","Falsetown")
station_ok6 =       MonitoringStation("T012","M012","Woolmere",(11.0,12.0),(0.1,0.9),"Fish Creek","Falsetown")



stationsall = [station_noname,station_nocoord,station_badcoord,station_norange,station_badrange,station_noriver,station_notown,station_badlevel,station_nolevel,
station_ok1,station_ok2,station_ok3,station_ok4,station_ok5,station_ok6]

for i in stationsall:
    i.latest_level=0.5



def test_stations_level_over_threshold():
    stationsgood = [station_noname,station_nocoord,station_badcoord,station_noriver,station_notown,
station_ok1,station_ok2,station_ok3,station_ok4,station_ok5,station_ok6]

    assert stations_level_over_threshold([],1)==[]

    assert type(stations_level_over_threshold(stationsgood,None,False))==TypeError
    assert type(stations_level_over_threshold("stationsgood",1,False))==TypeError

    assert len(stations_level_over_threshold(stationsgood,-999,False))==len(stationsgood)
    assert stations_level_over_threshold(stationsgood,999,False)==[]


def test_stations_highest_rel_level():

    stationsgood = [station_ok1,station_ok2,station_ok3,station_ok4,station_ok5,station_ok6]
    assert stations_highest_rel_level([],1)==[]

    assert type(stations_highest_rel_level("fish",1,False))==TypeError
    assert type(stations_highest_rel_level(stationsgood,"fish",False))==TypeError
    assert type(stations_highest_rel_level(stationsgood,-1,False)) == ValueError

    assert len(stations_highest_rel_level(stationsgood,1,False))==1
    assert len(stations_highest_rel_level(stationsgood,len(stationsgood),False))==len(stationsgood)
