
from floodsystem.plot import plot_water_levels_with_fit
from floodsystem.flood import stations_highest_rel_level
from floodsystem.stationdata import build_station_list, update_historic_water_levels
import datetime

def run():
    """Requirements for Task 2F"""
    # Build list of stations
    stations = build_station_list()
    sorted_stations = stations_highest_rel_level(stations,7) #obtains a list of the 5 stations with highest water level
                                                             # 7 stations were needed because (as of 28/01) two of the top 5 stations had invalid data
                                                             # polling the 7th station means that 5 are still plotted
    station_objects = [] #empty list to contain station objects with highest water level
    for i in sorted_stations:
        station_objects.append(i[0]) #adds station objects to list
    station_objects.reverse() #reverse the list so the lowest water levels are plotted first
    plot_water_levels_with_fit(station_objects,True,dt=10,p=4) #plots a graph of water levels with a polynomial order 4

if __name__ == "__main__":
    print("*** Task 2F: CUED Part IA Flood Warning System ***")
    run()
