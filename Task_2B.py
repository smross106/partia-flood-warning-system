from floodsystem.stationdata import build_station_list
from floodsystem.flood import stations_level_over_threshold
from floodsystem.station import inconsistent_typical_range_stations
from floodsystem.station import station_name

def run():
    """Requirements for Task 2B"""

    # Build list of stations
    stations = build_station_list() #builds a list of Monitoring Stations
    sorted_stations = stations_level_over_threshold(stations,0.8) #creates a list of tuples of monitoring stations with a relative level above 0.8
    

    for station in sorted_stations:
        print(station[0].name, station[1]) #prints out the name of the station and the relative water level
    
if __name__ == "__main__":
    print("*** Task 2B: CUED Part IA Flood Warning System ***")
    run()