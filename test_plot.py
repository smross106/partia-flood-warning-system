from floodsystem.stationdata import build_station_list
from floodsystem.station import inconsistent_typical_range_stations,station_name, MonitoringStation
from floodsystem.plot import plot_water_levels,plot_water_levels_with_fit

station_ok1 =       MonitoringStation("T007","M007","Kipthorpe",(-1.2,1.1),(0.1,0.9),"Babbling Brook","Fakefield")



def test_plot_water_levels():
    assert plot_water_levels([])==None

    assert type(plot_water_levels(station_ok1))==ValueError
    assert type(plot_water_levels("a"))==TypeError

def test_plot_water_levels_with_fit():
    assert plot_water_levels([])==None

    assert type(plot_water_levels_with_fit(station_ok1))==ValueError
    assert type(plot_water_levels_with_fit("a"))==TypeError

